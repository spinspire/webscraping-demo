CREATE TABLE `emp_post` (
	`link` VARCHAR(100) NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`designation` VARCHAR(50) NOT NULL,
	`pic` VARCHAR(255) NOT NULL,
	`bio` TEXT NULL,
	PRIMARY KEY (`link`)
)
COMMENT='Contains employee info'
ENGINE=InnoDB;
