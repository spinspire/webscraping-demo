Webscraping with Groovy
=======================
This project provides sample code on how to do web-scraping with Groovy (a JVM language).

Setup
=====
1. Install Groovy.
2. Create a MySQL database schema and run the db-schema.sql on it to create the require tables.
3. Copy config.properties.sample to config.properties and edit the settings there.
4. Make sure the webscraping.groovy file is executable. If not, run `chmod +x ./webscraping.groovy`
5. Run the program directly like any other executable.

Don't forget to review the source code ;-) The program takes command-line options. And can be
modified to your needs.
