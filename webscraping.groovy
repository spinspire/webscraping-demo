#!/usr/bin/env groovy

@Grab('org.jsoup:jsoup:1.8.3')
@Grab('mysql:mysql-connector-java:5.1.25')
@GrabConfig(systemClassLoader = true)

import groovy.util.CliBuilder
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

def scriptFile = new File(getClass().protectionDomain.codeSource.location.path)
def scriptName = scriptFile.getName()
def cli = new groovy.util.CliBuilder(usage: "$scriptName [options] [loan-ids]")
cli.with {
  s 'sleep between requests (in ms)', args: 1, argName: 'sleep', longOpt: 'sleep'
  l 'limit the max number of posts to fetch', args: 1, argName: 'limit', longOpt: 'limit'
}
options = cli.parse(args)

// default sleep interval
sleep_interval = 1000
if(options.s) {
  sleep_interval = Integer.parseInt(options.s)
}
limit = 5
if(options.l) {
  limit = Integer.parseInt(options.l)
}

def config = new Properties()
new File('config.properties').withInputStream {
  config.load(it)
}

def extractPostDetails(post) {
  if(sleep_interval > 0) {
    sleep(sleep_interval)
  }
  def conn = org.jsoup.Jsoup.connect(post['link'])
  conn.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36')
  def doc = conn.get()
  return extractPostDetails(post, doc)
}

def extractPostDetails(post, org.jsoup.nodes.Document doc) {
  def bio = doc.select('.views-field-field-bio').text()
  post['bio'] = bio
  println(post)
}

import groovy.sql.*
import java.sql.SQLException

db = Sql.newInstance(config.db_url, config.db_username, config.db_password, 'com.mysql.jdbc.Driver')
// hide failure log messages
// Sql.LOG.level = java.util.logging.Level.SEVERE
sqlInsert = null

def getPosts(String url) {
  def posts = []
  if(sleep_interval > 0) {
    sleep(sleep_interval)
  }
  def conn = org.jsoup.Jsoup.connect(url)
  conn.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36')
  def doc = conn.get()
  def view = doc.select('.who-we-are-view')
  def view_content = view.select('>div>ul')
  Elements lis = view_content.select('>li')
  for(Element li : lis) {
    def name = li.select('>.views-field-name').text()
    def link = li.select('a[href]').attr('abs:href')
    def designation = li.select('>.views-field-field-designation').text()
    def pic = li.select('>.views-field-user-picture img').attr('abs:src')
    def post = [
      name: name,
      link: link,
      designation: designation,
      pic: pic
    ]
    posts << post
  }
  return posts;
}

def savePost(post) {
  if(sqlInsert == null) {
    def cols = post.keySet()
    sqlInsert = 'REPLACE INTO emp_post (' + cols.join(',') + ') VALUES (' + cols.collect { ":$it" } .join(',') +')'
  }
  try {
    db.execute(sqlInsert, post)
  } catch(SQLException ex) {
    println(ex);
  }
}

// starting URL
def count = 0
def posts = getPosts('https://spinspire.com/who-we-are')
for(def post: posts) {
  extractPostDetails(post)
  savePost(post)
  if(++count >= limit) break
}
